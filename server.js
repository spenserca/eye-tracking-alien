'use strict';

const express = require('express');
const path = require('path');
const app = express();
const port = 8080;

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/index.css', (req, res) => {
    res.sendFile(path.join(__dirname + '/index.css'));
});

app.get('/index.js', (req, res) => {
    res.sendFile(path.join(__dirname + '/index.js'));
});

app.listen(port, () => console.log(`App listening on port: ${port}`));